#include <iostream>
#include <deque>
#include <fstream>
#include <algorithm>

int main(int, char **argv)
{
	std::deque<int64_t> nums;
	std::fstream ifs(argv[1], std::ios::in);
	int64_t input;
	const uint64_t Nth = atoi(argv[2]);
	for(int i=0; i<Nth; ++i)
	{
		ifs >> input;
		if(ifs.eof())
		{
			std::cerr << "No such number" << std::endl;
			return -1;
		}
		nums.push_back(input);
	}
	struct
	{
		bool operator()(int a, int b) const
		{
			return a < b;
		}
	}less;
	std::sort(nums.begin(), nums.end(), less);
	bool inserted;
	while(ifs >> input)
	{
		if(input < nums[0])
		{
			inserted = false;
			for(auto a = nums.cbegin(); a != nums.cend(); ++a)
			{
				if(input >= *a)
				{
					nums.pop_front();
					nums.insert(a, input);
					inserted = true;
					break;
				}
			}
			if(!inserted)
			{
				nums.pop_front();
				nums.push_back(input);
			}
		}
	}
	std::cout << nums[0] << std::endl;
}
