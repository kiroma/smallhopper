#include <random>
#include <fstream>
#include <chrono>

int main(int, char** argv)
{
	std::fstream ifs(argv[1], std::ios::out | std::ios::binary | std::ios::trunc);
	std::random_device rd;
	std::mt19937_64 mt(rd());
	for(long long i = 0; i < atoll(argv[2]); ++i)
	{
		ifs << static_cast<int64_t>(mt()) << " ";
	}
}
